using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    public GameManager buttonsUI;

    private void Start()
    {
        buttonsUI.gameOverUI.SetActive(false);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Finish Line"))
        {
            GameObject.Find("Level").GetComponent<PlatformMovement>().enabled = false;
            //TODO get reference from game events
            buttonsUI.levelCompleteUI.SetActive(true);
            Debug.Log("Player Won");
        }
        else if (collision.gameObject.CompareTag("Obstacle"))
        {
            Invoke(nameof(ActivateUI), 2f);
            GameObject.Find("Player").GetComponent<PlayerExplosion>().enabled = true;
            GameObject.Find("Level").GetComponent<PlatformMovement>().enabled = false;
            //TODO get reference from game events
        }
    }

    private void ActivateUI()
    {
        buttonsUI.gameOverUI.SetActive(true);
    }
}