using UnityEngine;

public class PlatformMovement : MonoBehaviour
{
    private Vector3 normalizeDirection;

    public Transform platformMoveTowards;
    public float platformMovingSpeed = 5f;

    private void Start()
    {
        normalizeDirection = (platformMoveTowards.position - transform.position).normalized;
    }

    private void Update()
    {
        transform.position += normalizeDirection * platformMovingSpeed * Time.deltaTime;
    }
}